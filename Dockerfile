FROM node:latest

RUN useradd -m docker
RUN npm install express-generator -g

USER docker
WORKDIR /home/docker/
EXPOSE 3000/tcp

COPY . .

ENTRYPOINT ["npm", "start"]